const parse = require("./table-parser.js");

describe("table.parser", () => {
  it("parses a table with no columns and no rows", () => {
    const table = "";
    expect(parse(table)).toEqual({
      header: [],
      rows: [],
    });
  });
  describe("parses a table with 1 column and 0 rows", () => {
    it("header of length 2 characters", () => {
      const table = "| id |";
      expect(parse(table)).toEqual({
        header: ["id"],
        rows: [],
      });
    });
    it("header length is 1", () => {
      const table = "| y |";
      expect(parse(table)).toEqual({
        header: ["y"],
        rows: [],
      });
    });
  });
  describe("parse a table with 1 column and 1 rows", () => {
    it("table with 1 column and 1 row", () => {
      const table = `
| id |
| 1 |`;
      expect(parse(table)).toEqual({
        header: ["id"],
        rows: ["1"],
      });
    });
  });
});
