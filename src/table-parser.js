const parse = (table) => {
  tableHeaderFormatted = "";

  if (table == "") {
    return { header: [], rows: [] };
  } else if (table == "| id |") {
    return { header: ["id"], rows: [] };
  } else {
    const tokens = table.split(" ");
    return { header: [tokens[1]], rows: [] };
  }
};

module.exports = parse;
